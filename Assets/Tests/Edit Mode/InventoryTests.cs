﻿using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;
using BuilderPatternExample;

namespace Tests
{
    public class InventoryTests
    {
        public class AddBag
        {
            [Test]
            public void Given_HasSpaceForOneBag_Then_AddsBag()
            {
                //var bag = new Bag(1);
                //var inventory = new Inventory(1);
                Bag bag = A.Bag;
                Inventory inventory = An.Inventory.WithCapacity(1);

                inventory.AddBag(bag);

                CollectionAssert.Contains(inventory.Bags, bag);
            }

            [Test]
            public void Given_HasSpaceForMultipleBags_Then_AddsBagOnce()
            {
                //var bag = new Bag(1);
                //var inventory = new Inventory(2);
                Bag bag = A.Bag;
                Inventory inventory = An.Inventory.WithCapacity(2);

                inventory.AddBag(bag);

                CollectionAssert.Contains(inventory.Bags, bag);
            }

            [Test]
            public void Given_HasNoSpaceForBags_Then_DoesNotAddBag()
            {
                //var bag1 = new Bag(1);
                //var bag2 = new Bag(1);
                //var inventory = new Inventory(1, bag1);
                Bag bag1 = A.Bag;
                Bag bag2 = A.Bag;
                Inventory inventory = An.Inventory.WithCapacity(2).WithBags(bag1, bag2);

                inventory.AddBag(A.Bag);

                CollectionAssert.Contains(inventory.Bags, bag1);
                CollectionAssert.Contains(inventory.Bags, bag2);
            }

            [Test]
            public void Given_HasSpaceForMultipleBags_Then_AddsBagsToFirstAvailableSlot()
            {
                //var bag1 = new Bag(1);
                //var bag2 = new Bag(1);
                //var bag3 = new Bag(1);
                //var inventory = new Inventory(4, bag1, null, bag3, null);
                Bag bag1 = A.Bag;
                Bag bag2 = A.Bag;
                Bag bag3 = A.Bag;
                Inventory inventory = An.Inventory.WithBags(bag1, null, bag3, null);

                inventory.AddBag(bag2);

                CollectionAssert.Contains(inventory.Bags, bag1);
                CollectionAssert.Contains(inventory.Bags, bag2);
                CollectionAssert.Contains(inventory.Bags, bag3);
                CollectionAssert.Contains(inventory.Bags, null);
            }
        }

        public class RemoveBag
        {
            [Test]
            public void Given_DoesNotHaveBag_Then_DoesNothing()
            {
                //var bag = new Bag(1);
                //var inventory = new Inventory(1);
                Bag bag = A.Bag;
                Inventory inventory = An.Inventory.WithCapacity(1);

                inventory.RemoveBag(bag);
                CollectionAssert.AreEqual(inventory.Bags, new object[] { null });
            }

            [Test]
            public void Given_HasBag_Then_RemovesBag()
            {
                //var bag1 = new Bag(1);
                //var bag2 = new Bag(1);
                //var inventory = new Inventory(1, bag1, bag2);
                Bag bag1 = A.Bag;
                Bag bag2 = A.Bag;
                Inventory inventory = An.Inventory.WithBags(bag1, bag2);

                inventory.RemoveBag(bag2);
                CollectionAssert.Contains(inventory.Bags, bag1);
                CollectionAssert.Contains(inventory.Bags, null);
            }
        }
    }
}
