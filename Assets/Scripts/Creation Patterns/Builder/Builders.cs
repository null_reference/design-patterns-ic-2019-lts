﻿using BuilderPatternExample;

namespace Tests
{
    public static class A
    {
        public static BagBuilder Bag => new BagBuilder();
    }

    public static class An
    {
        public static InventoryBuilder Inventory => new InventoryBuilder();
    }

    public class Builders
    {

    }

}
