﻿using System.Collections.Generic;

namespace BuilderPatternExample
{
    public class Inventory
    {
        private int _capacity;

        private List<Bag> _bags = new List<Bag>();

        public List<Bag> Bags{ get => _bags; }

        public Inventory(int capacity)
        {
            _capacity = capacity;
        }

        public Inventory(int capacity, params Bag[] bags)
        {
            if (capacity > 0)
                _capacity = capacity;
            else
                _capacity = bags.Length;

            for(int i=0; i<bags.Length; i++)
            {
                Bags.Add(bags[i]);
            }
        }

        public void AddBag(Bag bag)
        {
            if (_bags.Count == _capacity)
            {
                for (int i = 0; i < _bags.Count; i++)
                {
                    if (_bags[i] == null)
                    {
                        _bags[i] = bag;
                        return;
                    }
                }
            }
            else if (_bags.Count < _capacity)
            {
                _bags.Add(bag);
            }
        }

        public void RemoveBag(Bag bag)
        {
            for(int i=0; i<_bags.Count;i++)
            {
                if(_bags[i] == bag)
                {
                    _bags[i] = null;
                }
            }
        }
    }
}