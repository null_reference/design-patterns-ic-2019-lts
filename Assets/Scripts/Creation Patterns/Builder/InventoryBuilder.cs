﻿namespace BuilderPatternExample
{
    public class InventoryBuilder
    {
        private int _capacity;
        private Bag[] _bags;

        public InventoryBuilder WithCapacity(int capacity)
        {
            _capacity = capacity;
            return this;
        }
        
        public InventoryBuilder WithBags(params Bag[] bags)
        {
            _bags = bags;
            return this;
        }

        public Inventory Build()
        {
            return new Inventory(_capacity, _bags ?? new Bag[_capacity]);
        }

        public static implicit operator Inventory(InventoryBuilder builder)
        {
            return builder.Build();
        }
    }
}