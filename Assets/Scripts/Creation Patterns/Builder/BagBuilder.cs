﻿namespace BuilderPatternExample 
{
    public class BagBuilder
    {
        private int _capacity;
        public BagBuilder WithCapacity(int capacity)
        {
            _capacity = capacity;
            return this;
        }

        public Bag Build()
        {
            return new Bag(_capacity);
        }

        public static implicit operator Bag(BagBuilder builder)
        {
            return builder.Build();
        }
    }
}